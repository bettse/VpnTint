## Overview
Changes the OS X menu bar color based on if a VPN is connected.

## Advanced

Color values can be set using:

    defaults write org.ericbetts.VpnTint VpnColors -dict-add 'vpnname' '00FF00'

A color for the 'unconnected' state can be defined for the ''(empty string) connection

    defaults write org.ericbetts.VpnTint VpnColors -dict-add '' 'FF0000'

Caveat: If you have parenthesis in you vpn name, you need to use a [special quotation style](http://superuser.com/questions/321671/error-updating-plist-file-using-bash): `"\"`

    defaults write org.ericbetts.VpnTint VpnColors -dict "\"VPN (local)\"" '00ffff'



## Thanks

The following are resources I found particularly useful:
 * Mark from whimsicalifornia.com
 * http://stackoverflow.com/questions/15047338/is-there-a-nsnotificationcenter-notification-for-wifi-network-changes
 * http://stackoverflow.com/questions/11532144/how-to-detect-ip-address-change-on-osx-programmatically-in-c-or-c
 * http://codehackers.net/blog/?p=82
 * http://www.vpngate.net/en/howto_l2tp.aspx
 * http://stackoverflow.com/questions/8697205/convert-hex-color-code-to-nscolor

## Future work:

- [x] option to auto-launch app at boot
- [ ] a list of available VPNs
- [ ] option to enable tinting for the selected interface
- [ ] color wheel to select tint color
- [ ] live preview applied to the menu bar while uding the color wheel (the tint doesnt't match expected from the rgb value because of the translucency)
- [ ] handle other configured network interface (ethernet, wifi)
- [ ] handle different wifi AP names
- [ ] hierarchy to decide which tint to apply
- [ ] barber pole striped menu bar with alternating stripes for each active connection



