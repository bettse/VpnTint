//
//  TintedWindow.m
//  VpnTint
//
//  Created by Eric Betts on 8/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "TintedWindow.h"

@implementation TintedWindow


- (id) initWithContentRect: (NSRect) contentRect styleMask: (NSUInteger) aStyle backing: (NSBackingStoreType) bufferingType defer: (BOOL) flag
{
    self = [super initWithContentRect: contentRect
                            styleMask: NSBorderlessWindowMask
                              backing: bufferingType
                                defer: flag];
    if (self != nil)
    {
        [self reassert_location];
    }
    return self;
}

-(void) reassert_location
{
    self.opaque = NO;
    self.ignoresMouseEvents = YES;
    self.level = NSStatusWindowLevel;
    //https://developer.apple.com/library/mac/documentation/cocoa/conceptual/WinPanel/Articles/SettingWindowCollectionBehavior.html
    self.collectionBehavior = NSWindowCollectionBehaviorCanJoinAllSpaces | NSWindowCollectionBehaviorTransient | NSWindowCollectionBehaviorIgnoresCycle | NSWindowCollectionBehaviorFullScreenPrimary;
    [self makeKeyAndOrderFront:nil];

    CGFloat menuBarHeight = [[NSStatusBar systemStatusBar] thickness];
    NSRect screenRect = [[[NSScreen screens] objectAtIndex:0] frame];
    NSRect frame = NSMakeRect (0, screenRect.size.height - menuBarHeight,
                               screenRect.size.width, menuBarHeight);
    [self setFrame: frame display: YES animate: NO];
}

@end
