//
//  AppDelegate.h
//  VpnTint
//
//  Created by Eric Betts on 8/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "TintedWindow.h"
#import "NSColor+NSColorHexadecimalValue.h"
#import "NSImage+ImageAdditions.h"


@interface AppDelegate : NSObject <NSApplicationDelegate,NSUserNotificationCenterDelegate>

@property (assign) IBOutlet TintedWindow *window;

@property NSArray* palette;
@property float alpha;
@property NSSize icon_size;
@property NSMutableArray* watched_vpns;
@property NSString* active_service_id;
@property NSString* selected_vpn_name;

//Status Bar
@property IBOutlet NSMenu *statusMenu;
@property NSStatusItem *statusItem;
@property IBOutlet NSMenuItem *startAtLogin;
@property IBOutlet NSMenuItem *useBWIcon;

@end
