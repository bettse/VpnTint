//
//  NSColor+NSColorHexadecimalValue.h
//  VpnTint
//
//  Created by Eric Betts on 2/17/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//  https://developer.apple.com/library/mac/qa/qa1576/_index.html

#import <Cocoa/Cocoa.h>

@interface NSColor (NSColorHexadecimalValue)

-(NSString *)hexadecimalValueOfAnNSColor;

@end
