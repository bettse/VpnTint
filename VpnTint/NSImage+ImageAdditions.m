//
//  NSImage+ImageAdditions.m
//  VpnTint
//
//  Created by Eric Betts on 2/17/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//  http://stackoverflow.com/questions/11224131/creating-nsimage-from-nscolor

#import "NSImage+ImageAdditions.h"

@implementation NSImage (ImageAdditions)


+(NSImage *)swatchWithColor:(NSColor *)color size:(NSSize)size
{
    NSImage *image = [[NSImage alloc] initWithSize:size];
    [image lockFocus];
    [color drawSwatchInRect:NSMakeRect(0, 0, size.width, size.height)];
    [image unlockFocus];
    return image;
}
@end
