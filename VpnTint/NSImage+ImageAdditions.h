//
//  NSImage+ImageAdditions.h
//  VpnTint
//
//  Created by Eric Betts on 2/17/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//  http://stackoverflow.com/questions/11224131/creating-nsimage-from-nscolor

#import <Cocoa/Cocoa.h>

@interface NSImage (ImageAdditions)

+(NSImage *)swatchWithColor:(NSColor *)color size:(NSSize)size;

@end
