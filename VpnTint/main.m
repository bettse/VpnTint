//
//  main.m
//  VpnTint
//
//  Created by Eric Betts on 8/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
