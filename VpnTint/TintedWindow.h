//
//  TintedWindow.h
//  VpnTint
//
//  Created by Eric Betts on 8/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TintedWindow : NSWindow

-(void) reassert_location;

@end
