//
//  AppDelegate.m
//  VpnTint
//
//  Created by Eric Betts on 8/15/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//
//__bridge transfers a pointer between Objective-C and Core Foundation with no transfer of ownership.
//__bridge_retained or CFBridgingRetain casts an Objective-C pointer to a Core Foundation pointer and also transfers ownership to you. You are responsible for calling CFRelease or a related function to relinquish ownership of the object.
//__bridge_transfer or CFBridgingRelease moves a non-Objective-C pointer to Objective-C and also transfers ownership to ARC. ARC is responsible for relinquishing ownership of the object.

#import "AppDelegate.h"
@implementation AppDelegate

NSString *const BWIconKey = @"BlackAndWhiteMenuBarIcon";
NSString *const autoStartKey = @"startAtLogin";
NSString *const ServiceIDKey = @"ActiveServiceId";
NSString *const VpnColorsKey = @"VpnColors";

float const DefaultColorAlpha = 0.3f; //Alpha of NSColors that are created from hex string

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //Update how colors are stored:
    [self updateStoredColorFormat];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowWillClose:) name:NSWindowWillCloseNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveVpnEventNotification:) name:@"VpnEvent" object:nil];
    [NSApp setActivationPolicy:NSApplicationActivationPolicyAccessory]; //hide dock icon

    NSNotificationCenter *shared = [[NSWorkspace sharedWorkspace] notificationCenter];
    [shared addObserver:self selector: @selector(receiveWakeNote:) name: NSWorkspaceDidWakeNotification object: nil];
    [shared addObserver:self selector:@selector(screenWokeUp:) name: NSWorkspaceScreensDidWakeNotification object:nil];

    self.palette = @[
                     [[NSColor yellowColor] colorWithAlphaComponent:DefaultColorAlpha],
                     [[NSColor magentaColor] colorWithAlphaComponent:DefaultColorAlpha],
                     [[NSColor brownColor] colorWithAlphaComponent:DefaultColorAlpha],
                     [[NSColor cyanColor] colorWithAlphaComponent:DefaultColorAlpha],
                     [[NSColor blueColor] colorWithAlphaComponent:DefaultColorAlpha],
                     [[NSColor greenColor] colorWithAlphaComponent:DefaultColorAlpha]
                     ];
    self.icon_size = NSMakeSize(18.0, 18.0);
    self.watched_vpns = [[NSMutableArray alloc] init];
    self.selected_vpn_name = @"";

    NSColorPanel.pickerMask = NSColorPanelWheelModeMask;
    NSColorPanel.pickerMode = NSWheelModeColorPanel;
    
    [self addMenubarIcon];
    [self setLoginState];

    [self createVpnList];
    [self checkActiveVpns];
    
}

-(void)colorUpdate:(NSColorPanel*)colorPanel{
    NSColor* theColor = colorPanel.color;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    //NSLog(@"%s %@", __PRETTY_FUNCTION__, [theColor hexadecimalValueOfAnNSColor]);
    self.window.backgroundColor = theColor;

    //How do I figure out which one is being modified?
    NSString *vpn_name = self.selected_vpn_name;

    //Update color swatch next to menu itom
    NSMenuItem * theItem = [self.statusItem.menu itemWithTitle:vpn_name];
    theItem.image = [NSImage swatchWithColor:theColor size:self.icon_size];
    
    NSData *colorData=[NSArchiver archivedDataWithRootObject:theColor];
    NSDictionary *currentConfig = [prefs dictionaryForKey:VpnColorsKey];
    if (!currentConfig) currentConfig = [NSDictionary dictionary];
    NSMutableDictionary *newConfig = [currentConfig mutableCopy];
    [newConfig setObject:colorData forKey:vpn_name];
    [prefs setObject:newConfig forKey:VpnColorsKey];
    //NSLog(@"Setting user chosen color %@ for %@", [theColor hexadecimalValueOfAnNSColor], vpn_name);
    currentConfig = (NSDictionary*) newConfig;
}

-(void) readInPreferences
{
    //NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    /*
    if([prefs floatForKey:GlobalAlphaKey]) {
        self.alpha = [prefs floatForKey:GlobalAlphaKey];
    }
    */
    //https://developer.apple.com/library/mac/documentation/cocoa/Conceptual/DrawColor/Tasks/StoringNSColorInDefaults.html#//apple_ref/doc/uid/20001693-BAJBFGED
}

- (void)addMenubarIcon
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSImage* icon = [NSApp applicationIconImage];
    if([prefs boolForKey:BWIconKey]) {
        icon = [NSImage imageNamed:@"vpn_tint_bw"];
    }
    icon.size = self.icon_size;

    if (!self.statusItem) {
        self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
        self.statusItem.menu = self.statusMenu;
    }
    self.statusItem.image = icon;
    self.statusItem.highlightMode = YES;
    self.useBWIcon.state = [prefs boolForKey:BWIconKey];
}

-(void) handleVpnMenu:(id)sender
{
    //Look up service_id of clicked item
    NSMenuItem *item = (NSMenuItem*)sender;
    NSString *service_id = (NSString*)item.representedObject;
    NSString *vpn_name = [self getServiceName:service_id];
    NSLog(@"%s: %@", __PRETTY_FUNCTION__, vpn_name);
    
    self.selected_vpn_name = vpn_name;

    NSColorPanel *cp = [NSColorPanel sharedColorPanel];
    cp.showsAlpha = YES;
    cp.color = [self colorOfVpn:vpn_name];
    cp.continuous = YES;
    cp.floatingPanel = NO;
    cp.worksWhenModal = NO;
    cp.target = self;
    cp.action = @selector(colorUpdate:);
    cp.hidesOnDeactivate = NO;
    [cp orderFront:self];
    [cp orderFrontRegardless];
    [[NSApplication sharedApplication] orderFrontColorPanel:nil];

}

- (void)windowWillClose:(NSNotification *)notification {
    if ([notification.object isEqual:[NSColorPanel sharedColorPanel]]) {
        NSLog(@"%s: setting color panel action", __PRETTY_FUNCTION__);
        [[NSColorPanel sharedColorPanel] setAction:nil];
        [[NSColorPanel sharedColorPanel] setTarget:nil];
        [self checkActiveVpns];
    }
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    NSLog(@"%s: setting color panel action", __PRETTY_FUNCTION__);
    [[NSColorPanel sharedColorPanel] setAction:nil];
    [[NSColorPanel sharedColorPanel] setTarget:nil];
}

- (void) receiveVpnEventNotification:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    bool newState = [userInfo[@"newState"] boolValue];
    NSString *service_id = userInfo[@"service_id"];

    NSLog(@"%s: [%@] is %@", __PRETTY_FUNCTION__, service_id, newState ? @"On" : @"Off");

    //Create native notification
    if (![service_id isEqualToString:@""]) { //blank service_id is special case
        NSString *interface = [self getServiceName:service_id];
        [self nativeNotificationFor:interface new:newState];
    
        //Update menu item
        [self.statusMenu itemWithTitle:[self getServiceName:service_id]].state = newState;
    }

    if(!newState) { service_id = @"";}
    //Update tint
    [self setWindowTint:service_id];
}

- (void) setWindowTint:(NSString*)service_id {
    [self.window reassert_location];
    NSColor *newColor = [self colorOfVpn:[self getServiceName:service_id]];
    self.window.backgroundColor = newColor;
    //NSLog(@"WindowTint is now %@ (alpha: %f)", [newColor hexadecimalValueOfAnNSColor], [newColor alphaComponent]);
}

#pragma mark new notification stuff #pragma mark -
-(void) createVpnList {
    SCNetworkServiceRef	service;
    SCNetworkInterfaceRef interface;
    CFStringRef interfaceType;

    SCPreferencesRef prefs = SCPreferencesCreate(NULL, CFSTR("SCNetworkConnectionCopyAvailableServices"), NULL);
    if (prefs == NULL) { return; }
    
	CFArrayRef services = SCNetworkServiceCopyAll(prefs);
    CFRelease(prefs);

    [self.statusMenu insertItem:[NSMenuItem separatorItem] atIndex:[self.statusMenu numberOfItems] - 1];
    [self addToMenu:@""];
    
	if (services) {
		long count = CFArrayGetCount(services);
        //NSLog(@"%li services", count);
		for (int i = 0; i < count; i++) {
			service = CFArrayGetValueAtIndex(services, i);
            interface = SCNetworkServiceGetInterface(service);
            if (interface == NULL) { continue; }
            interfaceType = SCNetworkInterfaceGetInterfaceType(interface);
            if (CFEqual(interfaceType, kSCNetworkInterfaceTypeIPSec) ) {
                [self watchService:service];
            } else if (CFEqual(interfaceType, kSCNetworkInterfaceTypePPP)) {
                //NB: NetworkServiceGetInterface to NetworkInterfaceGetInterface
                interface = SCNetworkInterfaceGetInterface(interface);
                interfaceType = SCNetworkInterfaceGetInterfaceType(interface);
                if (CFEqual(interfaceType, kSCNetworkInterfaceTypeL2TP) ||
                    CFEqual(interfaceType, kSCNetworkInterfaceTypePPTP) ){
                    [self watchService:service];
                }
            }
		}
        CFRelease(services);
	}

    [self.statusMenu insertItem:[NSMenuItem separatorItem] atIndex:[self.statusMenu numberOfItems] - 1];
    //NSLog(@"VPN list: %@", self.watched_vpns);
}

-(void) watchService:(SCNetworkServiceRef) service
{
    NSString *service_id = (__bridge NSString*)SCNetworkServiceGetServiceID(service);
    //NSLog(@"%s: %@", __PRETTY_FUNCTION__, service_id);
    if (![self.watched_vpns containsObject:service_id]) {
        [self.watched_vpns addObject:service_id];
        [self addToMenu:service_id];
        
        SCNetworkConnectionContext context = { 0, (__bridge void*)self, NULL, NULL, NULL };
        SCNetworkConnectionRef connection = SCNetworkConnectionCreateWithServiceID(NULL, (__bridge CFStringRef)(service_id), networkStateCallback, &context);
        // setup watcher
        SCNetworkConnectionSetDispatchQueue(connection, dispatch_get_current_queue());
    }
}

- (void) addToMenu:(NSString*) service_id
{
    //NSLog(@"%s: %@", __PRETTY_FUNCTION__, service_id);
    NSColor *vpnColor = [self colorOfVpn:[self getServiceName:service_id]];

    NSMenuItem *newItem = [[NSMenuItem alloc ] initWithTitle:[self getServiceName:service_id] action:@selector(handleVpnMenu:) keyEquivalent:@""];
    newItem.representedObject = service_id;

    newItem.image = [NSImage swatchWithColor:vpnColor size:self.icon_size];
    [self.statusMenu insertItem:newItem atIndex:[self.statusMenu numberOfItems] - 1];
}


-(void) checkActiveVpns
{
    NSMutableArray *active = [NSMutableArray array];
    NSDictionary *userInfo;
    for (NSString* service_id in self.watched_vpns) {
        SCNetworkConnectionContext context = { 0, (__bridge void*)self, NULL, NULL, NULL };
        SCNetworkConnectionRef connection = SCNetworkConnectionCreateWithServiceID(NULL, (__bridge CFStringRef)(service_id), networkStateCallback, &context);
        SCNetworkConnectionStatus status = SCNetworkConnectionGetStatus(connection);
        BOOL on = (status == kSCNetworkConnectionConnected || status == kSCNetworkConnectionConnecting);
        if (on) {
            [active addObject:service_id];
        }
    }

    //NSLog(@"I see %li active Vpns", [active count]);
    if ([active count] == 0) {
        userInfo = @{ @"service_id": @"", @"newState": [NSNumber numberWithBool:NO] };
        [[NSNotificationCenter defaultCenter] postNotificationName: @"VpnEvent" object:nil userInfo:userInfo];
    } else {
        userInfo = @{ @"service_id": [active objectAtIndex:0], @"newState": [NSNumber numberWithBool:YES] };
        [[NSNotificationCenter defaultCenter] postNotificationName: @"VpnEvent" object:nil userInfo:userInfo];
    }
}

void networkStateCallback(SCNetworkConnectionRef connection, SCNetworkConnectionStatus status, void *info)
{
    //AppDelegate* self = (__bridge AppDelegate*) info;
    NSString *service_id = CFBridgingRelease(SCNetworkConnectionCopyServiceID(connection)); //(__bridge_transfer NSString *)
    BOOL on = (status == kSCNetworkConnectionConnected || status == kSCNetworkConnectionConnecting);
    //NSLog(@"%s: %@, %i", __PRETTY_FUNCTION__, service_id, on);
    NSDictionary *userInfo = @{
                               @"service_id": service_id,
                               @"newState": [NSNumber numberWithBool:on]
                               };
    [[NSNotificationCenter defaultCenter] postNotificationName: @"VpnEvent" object:nil userInfo:userInfo];
}

-(NSString*) getServiceName:(NSString*) service_id
{
    //Catch off state
    if ([service_id isEqualToString:@""]) return service_id;
    //Look up name
    SCPreferencesRef prefs = SCPreferencesCreate(NULL, CFSTR("SCNetworkConnectionCopyAvailableServices"), NULL);
    SCNetworkServiceRef service = SCNetworkServiceCopy(prefs, (__bridge CFStringRef)(service_id));
    CFRelease(prefs);
    if (!service) { NSLog(@"Error in %s getting name for service_id %@", __PRETTY_FUNCTION__, service_id); return @""; }
    NSString* name = (__bridge NSString *)(SCNetworkServiceGetName(service));
    CFRelease(service);
    //NSLog(@"%s: %@ -> %@", __PRETTY_FUNCTION__, service_id, name);
    return name;
}

#pragma mark Color stuff #pragma mark -

- (NSColor*) colorOfVpn:(NSString*) vpn_name
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary *currentConfig = [prefs dictionaryForKey:VpnColorsKey];
    if (!currentConfig) currentConfig = [NSDictionary dictionary];
    NSMutableDictionary *newConfig = [currentConfig mutableCopy];
    
    id encodedColor = [currentConfig objectForKey:vpn_name];
    
    if (vpn_name && encodedColor == nil){
        //Get next color
        NSColor *newColor = self.palette[[currentConfig count] % [self.palette count]];
        //Set to clear if empty string
        if ( [vpn_name isEqualToString:@""] ) {
            newColor = [NSColor clearColor];//Default to clear, but allows it to be overridden
        }
        //Encode to NSData
        encodedColor = [NSArchiver archivedDataWithRootObject:newColor];
        
        //Overwrite colors dict with updated copy
        [newConfig setObject:encodedColor forKey:vpn_name];
        [prefs setObject:newConfig forKey:VpnColorsKey];
        NSLog(@"Previously unseen VPN: setting %@ for %@", newColor, vpn_name);
        currentConfig = (NSDictionary*) newConfig;
    }
    
    //NSLog(@"Color dictionary: %@", currentConfig);
    
    return (NSColor*)[NSUnarchiver unarchiveObjectWithData:encodedColor];
}

/* Update user preference dictionary from using hex string for to color to NSData. */
- (void) updateStoredColorFormat {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary *currentConfig = [prefs dictionaryForKey:VpnColorsKey];
    if (!currentConfig) currentConfig = [NSDictionary dictionary];

    //Give us something we can work with
    NSMutableDictionary *newConfig = [currentConfig mutableCopy];

    for ( NSString *vpn_name in [newConfig allKeys]) {
        id encodedColor = [newConfig objectForKey:vpn_name];//Not sure if NSData or NSString
        if (![encodedColor isKindOfClass:[NSData class]]) {
            NSColor *vpnColor = [self colorWithHexColorString:(NSString*) encodedColor];

            NSLog(@"Updating the the color stored for %@ [%@]", vpn_name, vpnColor);
            //save it back to dictionary as NSData
            encodedColor = [NSArchiver archivedDataWithRootObject:vpnColor];
            [newConfig setObject:encodedColor forKey:vpn_name];
        }
    }

    [prefs setObject:newConfig forKey:VpnColorsKey];
}

- (NSColor*)colorWithHexColorString:(NSString*)inColorString
{
    NSColor* result = nil;
    unsigned colorCode = 0;
    unsigned char redByte, greenByte, blueByte;

    //Special case for interfaces that shouldn't tint the menu bar
    if (inColorString == nil || [inColorString isEqualToString:@""]){
        return [NSColor clearColor];
    }

    if (nil != inColorString)
    {
        NSScanner* scanner = [NSScanner scannerWithString:inColorString];
        (void) [scanner scanHexInt:&colorCode]; // ignore error
    }
    redByte = (unsigned char)(colorCode >> 16);
    greenByte = (unsigned char)(colorCode >> 8);
    blueByte = (unsigned char)(colorCode); // masks off high bits

    result = [NSColor
              colorWithCalibratedRed:(CGFloat)redByte / 0xff
              green:(CGFloat)greenByte / 0xff
              blue:(CGFloat)blueByte / 0xff
              alpha:DefaultColorAlpha];
    return result;
}

#pragma mark Handle various screen changes (sleep, new display, full screen app, etc) #pragma mark -
-(void)applicationDidChangeScreenParameters:(NSNotification*)aNotification {
    [self.window reassert_location];
}

-(void)screenWokeUp:(NSNotification*)aNotification {
    //Screen wake
    [self.window reassert_location];
}

- (void)receiveWakeNote: (NSNotification*) note {
    //System wake
    [self.window reassert_location];
    [self checkActiveVpns];
}

#pragma mark BWIcon #pragma mark -
- (IBAction)useBWIconToggle:(id)pid {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    self.useBWIcon.state = !self.useBWIcon.state;
    [prefs setBool:self.useBWIcon.state forKey:BWIconKey];
    [self addMenubarIcon];
}

#pragma mark Adding to login #pragma mark -

- (IBAction)startAtLoginToggle:(id)pid {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:!self.startAtLogin.state forKey:autoStartKey];
    [self setLoginState];
}

- (void) setLoginState {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs boolForKey:autoStartKey]) {
        [self addAppAsLoginItem];
    } else {
        [self deleteAppFromLoginItem];
    }
    self.startAtLogin.state = [prefs boolForKey:autoStartKey];
}

-(void) addAppAsLoginItem{
    NSString * appPath = [[NSBundle mainBundle] bundlePath];
    NSURL *url = [NSURL fileURLWithPath:appPath];
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    if (loginItems) {
        //Insert an item to the list.
        LSSharedFileListItemRef item = LSSharedFileListInsertItemURL(loginItems, kLSSharedFileListItemLast, NULL, NULL, (__bridge CFURLRef)(url), NULL, NULL);
        if (item){
            CFRelease(item);
        }
        CFRelease(loginItems);
    }
}

-(void) deleteAppFromLoginItem{
    UInt32 seedValue;
    NSString * appPath = [[NSBundle mainBundle] bundlePath];
    NSURL *url = [NSURL fileURLWithPath:appPath];
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    CFURLRef urlRef = (__bridge CFURLRef)url;
    
    if (loginItems) {
        NSArray  *loginItemsArray = (NSArray *)CFBridgingRelease(LSSharedFileListCopySnapshot(loginItems, &seedValue));
        for(int i = 0 ; i< [loginItemsArray count]; i++) {
            LSSharedFileListItemRef itemRef = (__bridge LSSharedFileListItemRef)([loginItemsArray objectAtIndex:i]);
            if (LSSharedFileListItemResolve(itemRef, 0, &urlRef, NULL) == noErr) {
                NSString *urlPath = [url path];
                if ([urlPath compare:appPath] == NSOrderedSame){
                    LSSharedFileListItemRemove(loginItems,itemRef);
                }
            }
        }
    }
}

#pragma mark NSUserNotificationCenterDelegate #pragma mark -
-(void) nativeNotificationFor:(NSString*)vpn_name new:(bool)state
{
    NSUserNotificationCenter* nc = [NSUserNotificationCenter defaultUserNotificationCenter];
    if(nc) {
        NSUserNotification *notification = [[NSUserNotification alloc] init];
        notification.title = vpn_name;
        notification.informativeText = [NSString stringWithFormat:@"%@ is now %@", vpn_name, state ? @"on" : @"off"];
        notification.soundName = NSUserNotificationDefaultSoundName;
        [nc deliverNotification:notification];
    }
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}



@end
